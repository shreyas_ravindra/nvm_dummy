/********************************************************
 *  This is an exercise which has been given by Qorvo	* 
 * For the purpose of interview 						*
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the data stored in   *
 * the NVM is correctly retrieved or not 				*
 *														* 
 * Author : Shreyas Ravindra 							*
 * Organization : Qorvo Interview Assignment		    *
 *                                               		* 
 ********************************************************
 */ 
#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdio.h>

/*This file would have a list of Test functions which would be used*/
/* for testing the NVM driver */ 

/*following is the list of test functions which would be used for testing*/
/* the NVM driver. This would basically consists of test vectors which are */
/* passed through the NVM driver to store and retrieve the data */ 
/* Note :- All new Tests should be added here */  
void test_storing_one_byte(); 
void test_storing_two_byte(); 
void test_storing_four_byte(); 
void test_storing_eight_byte(); 
void test_data_retrieval_one_byte(); 
void test_data_retrieval_two_byte(); 
void test_data_retrieval_four_byte(); 
void test_data_retrieval_eight_byte(); 
void test_storing_structure_20_bytes(); 
void test_data_retrieval_20_bytes();
void test_storing_structure_30_bytes();
void test_data_retrieval_30_bytes(); 
void test_storing_structure_50_bytes(); 
void test_data_retrieval_50_bytes(); 
void test_storing_structure_60_bytes();
void test_data_retrieval_60_bytes();
void test_example_driver_1();
void test_example_driver_2();
void test_incorrect_packet_size_storage();
void test_storing_data_with_zero_length(); 
void test_storing_data_with_buffer_null();
void test_try_overwriting_data();
void test_retrieval_with_null_pointers();
void test_retrieval_from_memory_which_is_unavailable();
/*The following tests would corrupt some locations in the memory and */
/*see if data can be retrieved successfully or not */
void test_checksum_corruption();
void test_data_corruption_in_memory();



#endif /* __MAIN_H__*/ 
