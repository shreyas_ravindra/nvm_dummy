/********************************************************
 *  This is an exercise which has been given by Qorvo	* 
 * For the purpose of interview 						*
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the data stored in   *
 * the NVM is correctly retrieved or not 				*
 *														* 
 * Author : Shreyas Ravindra 							*
 * Organization : Qorvo Interview Assignment			*
 *                                               		* 
 ********************************************************
 */ 
#include "nvm.h"

/********************************************************
 * This section contains a decleration of static 
 * functions. These are functions which are used by the 
 * driver only. These are not accessed by the function
 * outside of the driver. Description of the function
 * will be in the 
 ********************************************************
 */
static UInt8 gpNvm_CalculateChecksum(UInt8* data_buffer);
  

/*create a static ram page for the purpose of work*/
static t_ram_page ramWxPage; 
static t_page_occupied pageData; 
static bool isNVMCreated = false; 

/*
 * This API calculates the checksum for storing the 
 * data in the memory
 *
 */ 
static UInt8 gpNvm_CalculateChecksum(UInt8* data_buffer)
{
	int i = 0;
	int sum =0; 
	UInt8 checksum = 0x0;  
	if(NULL == data_buffer)
	{
		PRNT(("\nERROR:Null pointer encountered")); 
		return NVM_NULL_POINTER_SUPPLIED; 
	}
	else
	{
		for(i=0; i<61; i++)
		{
			sum = sum + data_buffer[i]; 
		}
	}
	checksum = sum & NVM_GET_BYTE_MASK; 
	return checksum; 
}


/* This is the API which is used by application to store the data 
 * in the NVM. The description of parameters 
 * attrId - [IN]8-bit page where the data has to be stored 
 * 			Value greater than 0xFF will be rejected 
 * length - [IN]Amount of data to be stored. Maximum length allowed is 60 bytes
 * 			if more data is needed, Application has to make a call again
 *			with the next buffer of 60 Bytes 
 * pValue - [IN/OUT]Pointer to the buffer, which has to be stored in NVM
 *
 */ 
gpNvm_Result gpNvm_SetAttribute(gpNvm_AttrId attrId,UInt8 length,UInt8* pValue)
{
	/*local variables for handling*/
	FILE *fptr; 
	long int offset = 0x0; 
	int i =0; 
	int successful_written = 0; 
	/*first perform the sanity check of the data */
	if(attrId >TOTAL_PAGES)
	{
		PRNT(("\nERROR:attrId is incorrect ")); 
		return NVM_ERROR_ATTRIBUTE_ID_INCORRECT; 
	}
	if(length > MAX_DATA_PAYLOAD)
	{
		PRNT(("\nERROR:Packet size is incorrect ")); 
		return NVM_ERROR_PACKET_SIZE_INCORRECT; 
	}
	if(length == 0x0)
	{
		PRNT(("\n WARNING: Nothing to write")); 
		return NVM_SUCCESS; 
	}
	if(NULL == pValue)
	{
		PRNT(("\nERROR:Null pointer encountered")); 
		return NVM_NULL_POINTER_SUPPLIED; 
	}
	if(pageData.pg_occ[attrId])
	{
		PRNT(("\n ERROR: page is already marked as occupied"));
		return NVM_WRITE_ERROR_PAGE_OCCUPIED;
	}
	/*During writing, the file has to be in append mode else*/
	/* the data does get over-written                       */
	if(false == isNVMCreated)
	{
		fptr = fopen("nvm.bin","wb"); 
		if(NULL == fptr)
		{
			PRNT(("\nERROR:file open failed"));
			return NVM_FILE_OPEN_FAILED; 
		}
		isNVMCreated = true; 
	}
	else
	{
		fptr = fopen("nvm.bin","ab"); 
		if(NULL == fptr)
		{
			PRNT(("\nERROR:file open failed"));
			return NVM_FILE_OPEN_FAILED; 
		}
	}

	/*clear the page template now*/
	memset(&ramWxPage, 0x0, sizeof(ramWxPage)); 
	/*Copy the data in the RAM template */
	ramWxPage.attrId = attrId; 
	ramWxPage.size = length; 
	memcpy(ramWxPage.data_buffer,pValue,length); 
	ramWxPage.checksum = gpNvm_CalculateChecksum(ramWxPage.data_buffer); 
	/*Move the cursor to the write position in the file*/
	for(i=0; i< attrId; i++)
	{
		offset+= PAGE_SKIP_SIZE; 
	}
	fseek(fptr,offset,SEEK_SET);
	/*All the data is available now write the data in the file */
	successful_written= fwrite(&ramWxPage,sizeof(ramWxPage),FWRITE_MEMBERS_WRITTEN,fptr); 
	if(successful_written != FWRITE_MEMBERS_WRITTEN)
	{
		PRNT(("\nERROR: Write Error has occured"));
		fclose(fptr);
		return NVM_WRITE_ERROR; 
	}
	/* If everything is successful, report success*/
	/* Also mark the page as busy*/
	pageData.pg_occ[attrId] = true; 
	fclose(fptr);
	return NVM_SUCCESS; 

}

/* This is the API which is used by application to get the data 
 * from the NVM. The descriptino of parameters 
 * attrId - [IN]8-bit page from where the data has to be retrieved
 *			Value greater than 0xFF will be rejected
 * pLength - [OUT]Returns the number of bytes which have been successfully read 
 *			from the NVM flash 
 * pValue  - [IN/OUT]Data is stored at the location pointed by this pointer
 * 
 */ 
gpNvm_Result gpNvm_GetAttribute(gpNvm_AttrId attrId,UInt8* pLength,UInt8* pValue)
{
	/*local variables for handling*/
	FILE *fptr; 
	int successful_read = 0; 
	long int offset = 0x0; 
	int i =0; 
	/*first perform the sanity check of the data */
	if(attrId >TOTAL_PAGES)
	{
		PRNT(("\nERROR:attrId is incorrect ")); 
		return NVM_ERROR_ATTRIBUTE_ID_INCORRECT; 
	}
	if(NULL== pLength || NULL == pValue)
	{
		PRNT(("\n ERROR: Null pointer encountered"));
		return NVM_NULL_POINTER_SUPPLIED; 
	}
	if(false == pageData.pg_occ[attrId])
	{
		PRNT(("\n ERROR: Trying to access empty location"));
		return NVM_READ_NOT_POSSIBLE_EMPTY;
	}
	/*open the file to read the data*/
	fptr = fopen("nvm.bin","rb"); 
	if(NULL == fptr)
	{
		PRNT(("\nERROR:file open failed"));
		return NVM_FILE_OPEN_FAILED; 
	}
	for(i=0; i< attrId; i++)
	{
		offset+= PAGE_SKIP_SIZE; 
	}
	/* Look for the currect location from which the data needs to be retrieved*/
	/* in the file */ 
	fseek(fptr,offset,SEEK_SET); 
	/* Perform a read of data from that location */
	successful_read = fread(&ramWxPage, sizeof(ramWxPage),FREAD_MEMBERS_READ,fptr); 
	if(successful_read != FREAD_MEMBERS_READ)
	{
		PRNT(("\n ERROR: Reading data from Memory "));
		fclose(fptr);
		return NVM_READ_ERROR;
	}
	/* if the check sums are correct then proceed else return error*/
	if(gpNvm_CalculateChecksum(ramWxPage.data_buffer) == ramWxPage.checksum)
	{
		*(pLength) = ramWxPage.size;
		memcpy(pValue,ramWxPage.data_buffer,ramWxPage.size);
	}
	else
	{
		PRNT(("\n ERROR: Checksum ERROR data corruption"));
		fclose(fptr);
		return NVM_ERROR_CHECKSUM_CORRUPT;
	}
	fclose(fptr);
	return NVM_SUCCESS; 
}

