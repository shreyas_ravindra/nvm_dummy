/********************************************************
 *  This is an exercise which has been given by Qorvo	* 
 * For the purpose of interview 						*
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the data stored in   *
 * the NVM is correctly retrieved or not 				*
 *														* 
 * Author : Shreyas Ravindra 							*
 * Organization : Qorvo Interview Assignment			*
 *                                               		* 
 ********************************************************
 */ 


/* Type Definitions as is specified in the Requirement */ 

#ifndef __NVM_H__
#define __NVM_H__

#include <stdio.h>
#include <stdbool.h>
#include <string.h> //for memset 

#if PRINT_DEBUG_MESSAGES_IN_DRIVER
#define PRNT(a) printf a
#else 
#define PRNT(a) (void)0x0
#endif /*PRINT_DEBUG_MESSAGES_IN_DRIVER*/ 

/*define the type definitions for */
typedef unsigned char UInt8;  
typedef unsigned short int UInt16; 
typedef UInt8 gpNvm_AttrId;
typedef UInt8 gpNvm_Result;

#define NVM_SUCCESS 						0x0
#define NVM_GET_BYTE_MASK					0x000000FF
#define NVM_ERROR_MAXSIZE_REACHED 			0xFF 
#define NVM_ERROR_PACKET_SIZE_INCORRECT		(NVM_ERROR_MAXSIZE_REACHED -1)
#define NVM_ERROR_ATTRIBUTE_ID_INCORRECT    (NVM_ERROR_PACKET_SIZE_INCORRECT-1)
#define NVM_NULL_POINTER_SUPPLIED			(NVM_ERROR_ATTRIBUTE_ID_INCORRECT-1)
#define NVM_ERROR_CHECKSUM_CORRUPT 			(NVM_NULL_POINTER_SUPPLIED -1)
#define NVM_FILE_OPEN_FAILED				(NVM_ERROR_CHECKSUM_CORRUPT - 1)
#define NVM_WRITE_ERROR						(NVM_FILE_OPEN_FAILED - 1)
#define NVM_WRITE_ERROR_PAGE_OCCUPIED		(NVM_WRITE_ERROR - 1)
#define NVM_READ_NOT_POSSIBLE_EMPTY			(NVM_WRITE_ERROR_PAGE_OCCUPIED -1)
#define NVM_READ_ERROR						(NVM_READ_NOT_POSSIBLE_EMPTY - 1)

#define FWRITE_MEMBERS_WRITTEN 				0x1
#define FREAD_MEMBERS_READ     				FWRITE_MEMBERS_WRITTEN 


/* Assume page size is 64 bytes each */ 
/* This means the maximum size of RAM */ 
/* Before storing it on flash is 64 bytes */ 
#define PAGE_SIZE 				0x40 

/* Number of Pages available */ 
/* Since attribute Id is UInt8 */ 
/* The number of pages which is available */
/* becomes 256 pages                      */ 
#define TOTAL_PAGES				0xFF
#define PAGE_SKIP_SIZE 			PAGE_SIZE 
#define MAX_DATA_PAYLOAD		0x3D  /*61 bytes of data payload only allowed */

/* This can be considered as a template used to store
 * or retrieve the data from NVM
 */
typedef struct __attribute__((packed)) ram_page {
	UInt8 attrId; 
	UInt8 size; 
	UInt8 data_buffer[61]; 
	UInt8 checksum;  
}t_ram_page;

/* This is used to track if a particular attribute is 
 * already occupied or not
 * 
*/
typedef struct __attribute__((packed)) page_occupied {
	bool pg_occ[256]; 
}t_page_occupied; 

/*
 * Every page will have an attribute_Id, data_size
 * payload and 1-byte Checksum. So ineffect the 
 * data which can be stored is only 64-3 i.e. 
 * 61 bytes. If the data is greater than 
 * 61 bytes then another page needs to be used
 */

/*
 * For the purpose of this exercise assume max data
 * size which can be handled by the driver 
 * is only 61 bytes. If the data size is greater
 * than 61 bytes, then application has to send two 
 * requests for storing the data. The driver can 
 * accept only packets which are upto 60 bytes
 */ 

/* This is the API which is used by application to store the data 
 * in the NVM. The description of parameters 
 * attrId - [IN]8-bit page where the data has to be stored 
 * 			Value greater than 0xFF will be rejected 
 * length - [IN]Amount of data to be stored. Maximum length allowed is 61 bytes
 * 			if more data is needed, Application has to make a call again
 *			with the next buffer of 61 Bytes 
 * pValue - [IN/OUT]Pointer to the buffer, which has to be stored in NVM
 *
 */ 
gpNvm_Result gpNvm_SetAttribute(gpNvm_AttrId attrId,UInt8 length,UInt8* pValue);

/* This is the API which is used by application to get the data 
 * from the NVM. The descriptino of parameters 
 * attrId - [IN]8-bit page from where the data has to be retrieved
 *			Value greater than 0xFF will be rejected
 * pLength - [OUT]Returns the number of bytes which have been successfully read 
 *			from the NVM flash 
 * pValue  - [IN/OUT]Data is stored at the location pointed by this pointer
 * 
 */ 
gpNvm_Result gpNvm_GetAttribute(gpNvm_AttrId attrId,UInt8* pLength,UInt8* pValue);


#endif  /* __NVM_H__*/


