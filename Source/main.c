/********************************************************
 *  This is an exercise which has been given by Qorvo	* 
 * For the purpose of interview 						*
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the data stored in   *
 * the NVM is correctly retrieved or not 				*
 *														* 
 * Author : Shreyas Ravindra 							*
 * Organization : Qorvo Interview Assignment			*
 *                                               		* 
 ********************************************************
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "main.h"
#include "nvm.h"

/*global statics which will be used to sort of get the */
/* attribute id for an attribute which is being stored */ 
/* in the NVM 										   */
static gpNvm_AttrId t_attribute_id = 0; 



/********************************************************
 *  This is the main program which is the starting  	* 
 * 	point of execution of the test cases				*
 *                                               		* 
 ********************************************************
 */ 
int main(void)
{
	printf("\n***   WELCOME TO THE TEST CASE EXECUTION TO CHECK NVM DRIVER   ***");
	printf("\n 1. Basic Storge Test uint8 "); 
	test_storing_one_byte(); 
	printf(".....PASSED\n");

	printf("\n 2. Basic Storage Test uint16 "); 
	test_storing_two_byte();
	printf(".....PASSED\n");

	printf("\n 3. Basic Storage Test uint32 ");
	test_storing_four_byte();
	printf(".....PASSED\n");

	printf("\n 4. Basic Storage Test uint64 "); 
	test_storing_eight_byte(); 
	printf(".....PASSED\n");

	printf("\n 5. Basic Data Retrieval Test uint8"); 
	test_data_retrieval_one_byte();
	printf(".....PASSED\n");

	printf("\n 6. Basic Data Retrieval Test uint16"); 
	test_data_retrieval_two_byte(); 
	printf(".....PASSED\n");

	printf("\n 7. Basic Data Retrieval Test uint32"); 
	test_data_retrieval_four_byte(); 
	printf(".....PASSED\n");

	printf("\n 8. Basic Data Retrieval Test uint64"); 
	test_data_retrieval_eight_byte(); 
	printf(".....PASSED\n");

	printf("\n 9. Store 20 Byte Structure"); 
	test_storing_structure_20_bytes(); 
	printf(".....PASSED\n");

	printf("\n 10. Retrieve 20 Byte Structure"); 
	test_data_retrieval_20_bytes(); 
	printf(".....PASSED\n");

	printf("\n 11. Store 30 Byte Structure"); 
	test_storing_structure_30_bytes(); 
	printf(".....PASSED\n");

	printf("\n 12. Retrieve 30 Byte Structure"); 
	test_data_retrieval_30_bytes(); 
	printf(".....PASSED\n");

	printf("\n 13. Store 50 Byte Structure"); 
	test_storing_structure_50_bytes();
	printf(".....PASSED\n");

	printf("\n 14. Retrieve 50 Byte Structure");
	test_data_retrieval_50_bytes();
	printf(".....PASSED\n");

	printf("\n 15. Store 60 Byte Structure"); 
	test_storing_structure_60_bytes();
	printf(".....PASSED\n");

	printf("\n 16. Retrieve 60 Byte Structure");
	test_data_retrieval_60_bytes();
	printf(".....PASSED\n");

	printf("\n 17. Application Driver - Storage 256Bytes"); 
	test_example_driver_1();
	printf(".....PASSED\n");

	printf("\n 18. Application Driver - Retrieve 256Bytes");
	test_example_driver_2();
	printf(".....PASSED\n"); 

	printf("\n 19. Test Storing Incorrect Packet Size 64 Bytes");
	test_incorrect_packet_size_storage();
	printf(".....PASSED\n"); 

	printf("\n 20. Test Storing Data with length provided as 0 Bytes"); 
	test_storing_data_with_zero_length();
	printf(".....PASSED\n"); 

	printf("\n 21. Test Storing Data by passing Null Buffer"); 
	test_storing_data_with_buffer_null();
	printf(".....PASSED\n");

	printf("\n 22. Try overwriting data "); 
	test_try_overwriting_data();
	printf(".....PASSED\n");

	printf("\n 23. Try passing Null pointers to data retrieval"); 
	test_retrieval_with_null_pointers();
	printf(".....PASSED\n");

	printf("\n 24. Data Retrieval from memory which is unavailable");
	test_retrieval_from_memory_which_is_unavailable();
	printf(".....PASSED\n");

	printf("\n 25. Test Checksum Corruption");
	test_checksum_corruption();
	printf(".....PASSED\n");

	printf("\n 26. Test Data Corruption");
	test_data_corruption_in_memory(); 
	printf(".....PASSED\n"); 

	printf("\n \n \n");
 
	return 0x0; 
}

/********************************************************
 *  This is the area which has all the tests which makes*
 *  use of the NVM APIs to store and retrieve data      *
 *                                               		* 
 ********************************************************
 */ 
void test_storing_one_byte()
{
	UInt8 oneByte = 0xDE; 
	gpNvm_Result result = NVM_SUCCESS; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x1, &oneByte); 
	assert(result == NVM_SUCCESS); 

}

void test_storing_two_byte()
{
	UInt16 twoByte = 0xDEAD; 
	UInt8 buffer[2] = {0xff}; 
	memcpy(buffer,&twoByte, sizeof(twoByte));
	gpNvm_Result result = NVM_SUCCESS; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x2, buffer); 
	assert(result == NVM_SUCCESS);
}

void test_storing_four_byte()
{
	unsigned int fourByte = 0xDEADBEEF; 
	UInt8 buffer[4];
	memset(buffer,0xff,4);
	memcpy(buffer,&fourByte, sizeof(fourByte));
	gpNvm_Result result = NVM_SUCCESS; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x4, buffer); 
	assert(result == NVM_SUCCESS);
}

void test_storing_eight_byte()
{
	unsigned long int eightByte= 0xDEADBEEF10010000; 
	UInt8 buffer[8];
	memset(buffer,0xff,8);
	memcpy(buffer,&eightByte,sizeof(eightByte));
	gpNvm_Result result = NVM_SUCCESS; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x8,buffer); 
	assert(result == NVM_SUCCESS);
}

void test_data_retrieval_one_byte()
{
	UInt8 oneByte = 0xBA; 
	gpNvm_Result result = NVM_SUCCESS; 
	UInt8 pValue = 0x0; 
	UInt8 pLength = 0x0; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x1, &oneByte); 
	assert(result == NVM_SUCCESS);
	result = gpNvm_GetAttribute(t_attribute_id,&pLength,&pValue); 
	/*Check if the data is indeed correct*/
	assert(result == NVM_SUCCESS);
	assert(pLength == 0x1); 
	assert(pValue == 0xBA); 
}

void test_data_retrieval_two_byte()
{
	UInt16 twoByte = 0xB00B; 
	UInt8 Txbuffer[2];
	memset(Txbuffer,0xff,2);
	memcpy(Txbuffer,&twoByte,sizeof(twoByte));
	gpNvm_Result result = NVM_SUCCESS; 
	UInt8 buffer[2]; 
	memset(buffer,0x0,2); 
	UInt8 pLength = 0x0; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x2, Txbuffer); 
	assert(result == NVM_SUCCESS);
	result = gpNvm_GetAttribute(t_attribute_id,&pLength,buffer); 
	/*Check if the data is indeed correct*/
	assert(result == NVM_SUCCESS);
	assert(pLength == 0x2); 
	assert(buffer[0] == 0x0B);
	assert(buffer[1] == 0xB0);  
}

void test_data_retrieval_four_byte()
{
	unsigned int fourByte = 0xB00BDEAD; 
	UInt8 Txbuffer[4];
	memset(Txbuffer,0xff,4);
	memcpy(Txbuffer,&fourByte,sizeof(fourByte));
	gpNvm_Result result = NVM_SUCCESS; 
	UInt8 buffer[4]; 
	memset(buffer,0x0,4); 
	UInt8 pLength = 0x0; 
	result = gpNvm_SetAttribute(++t_attribute_id,0x4, Txbuffer); 
	assert(result == NVM_SUCCESS);
	result = gpNvm_GetAttribute(t_attribute_id,&pLength,buffer); 
	/*Check if the data is indeed correct*/
	assert(result == NVM_SUCCESS);
	assert(pLength == 0x4); 
	assert(buffer[0] == 0xAD);
	assert(buffer[1] == 0xDE);  
	assert(buffer[2] == 0x0B); 
	assert(buffer[3] == 0xB0); 
}

void test_data_retrieval_eight_byte()
{
	unsigned long int eightByte = 0xB00BDEADBEAFDEAD; 
	UInt8 Txbuffer[8];
	memset(Txbuffer,0xff,8);
	memcpy(Txbuffer,&eightByte,sizeof(eightByte));
	gpNvm_Result result = NVM_SUCCESS; 
	UInt8 buffer[8];
	memset(buffer,0x0,8); 
	UInt8 pLength = 0x0;
	result = gpNvm_SetAttribute(++t_attribute_id,0x8, Txbuffer); 
	assert(result == NVM_SUCCESS);
	result = gpNvm_GetAttribute(t_attribute_id,&pLength,buffer); 
	/*Check if the data is indeed correct*/
	assert(result == NVM_SUCCESS);
	assert(pLength == 0x8);  
	assert(buffer[0] == 0xAD);
	assert(buffer[1] == 0xDE);
	assert(buffer[2] == 0xAF); 
	assert(buffer[3] == 0xBE); 
	assert(buffer[4] == 0xAD); 
	assert(buffer[5] == 0xDE); 
	assert(buffer[6] == 0x0B); 
	assert(buffer[7] == 0xB0); 
}

void test_storing_structure_20_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[9]; 
		UInt8 magic1; 
		UInt8 magic2; 
	}t_numbers; 
	int i = 0; 
	UInt8 Txbuffer[20];
	gpNvm_Result result;
	t_numbers dataNumbers;
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	dataNumbers.magic1 = 0x24; 
	dataNumbers.magic2 = 0x15; 
	memset(Txbuffer,0xff,20);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result = gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer); 
	assert(result == NVM_SUCCESS);
}

void test_data_retrieval_20_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[9]; 
		UInt8 magic1; 
		UInt8 magic2; 
	}t_numbers; 
	int i = 0; 
	UInt8 Txbuffer[20];
	t_numbers dataNumbers;
	t_numbers rxdataNumbers;
	gpNvm_Result result; 
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	UInt8 rxDataSize = 0x0; 
	UInt8 rxBuffer[20] = {0}; 
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	dataNumbers.magic1 = 0x24; 
	dataNumbers.magic2 = 0x15; 
	memset(Txbuffer,0xff,20);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result = gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer); 
	assert(result == NVM_SUCCESS);

	result = gpNvm_GetAttribute(t_attribute_id, &rxDataSize, rxBuffer); 
	assert(result == NVM_SUCCESS); 
	assert(rxDataSize == dataSize); 
	memcpy(&rxdataNumbers,rxBuffer,sizeof(rxdataNumbers)); 
	/*Perform the data sanity check */
	for(i=0; i<9; i++)
	{
		assert(dataNumbers.numbers[i] == rxdataNumbers.numbers[i]); 
	}
	assert(dataNumbers.magic1 == rxdataNumbers.magic1); 
	assert(dataNumbers.magic2 == rxdataNumbers.magic2); 
}

void test_storing_structure_30_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[10]; 
		UInt8 magic[10];  
	}t_numbers; 
	int i = 0; 
	UInt8 Txbuffer[30];
	t_numbers dataNumbers;
	gpNvm_Result result;
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	memset(Txbuffer,0xff,30);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result = gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer); 
	assert(result == NVM_SUCCESS);
}

void test_data_retrieval_30_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[10]; 
		UInt8 magic[10];  
	}t_numbers; 
	int i = 0; 
	UInt8 Txbuffer[30];
	t_numbers dataNumbers;
	t_numbers rxdataNumbers; 
	gpNvm_Result result;
	UInt8 rxDataSize = 0x0; 
	UInt8 rxBuffer[30] = {0}; 
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	memset(Txbuffer,0xff,30);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result= gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer);
	assert(result == NVM_SUCCESS);

	result = gpNvm_GetAttribute(t_attribute_id, &rxDataSize, rxBuffer); 
	assert(result == NVM_SUCCESS); 
	assert(rxDataSize == dataSize); 
	memcpy(&rxdataNumbers,rxBuffer,sizeof(rxdataNumbers)); 
	/*Perform the data sanity check */
	for(i=0; i<9; i++)
	{
		assert(dataNumbers.numbers[i] == rxdataNumbers.numbers[i]); 
		assert(dataNumbers.magic[i] == rxdataNumbers.magic[i]); 
	}

}

void test_storing_structure_50_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[10]; 
		UInt8 magic[10];  
		UInt16 products[10]; 
	}t_numbers; 
	int i = 0; 
	gpNvm_Result result;
	UInt8 Txbuffer[50];
	t_numbers dataNumbers;
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	for(i=0; i<9; i++)
	{
		dataNumbers.products[i] = 0xBEEF; 
	}
	memset(Txbuffer,0xff,50);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result= gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer);
	assert(result == NVM_SUCCESS);
}

void test_data_retrieval_50_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		UInt16 numbers[10]; 
		UInt8 magic[10]; 
		UInt16 products[10];  
	}t_numbers; 
	int i = 0; 
	t_numbers dataNumbers;
	t_numbers rxdataNumbers; 
	gpNvm_Result result;
	UInt8 Txbuffer[50];
	UInt8 rxDataSize = 0x0; 
	UInt8 rxBuffer[50] = {0}; 
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	for(i=0; i<9; i++)
	{
		dataNumbers.products[i] = 0xBEEF; 
	}
	memset(Txbuffer,0xff,50);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result= gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer); 
	assert(result == NVM_SUCCESS);

	result = gpNvm_GetAttribute(t_attribute_id, &rxDataSize, rxBuffer); 
	assert(result == NVM_SUCCESS); 
	assert(rxDataSize == dataSize); 
	memcpy(&rxdataNumbers,rxBuffer,sizeof(rxdataNumbers)); 
	/*Perform the data sanity check */
	for(i=0; i<9; i++)
	{
		assert(dataNumbers.numbers[i] == rxdataNumbers.numbers[i]); 
		assert(dataNumbers.magic[i] == rxdataNumbers.magic[i]); 
		assert(dataNumbers.products[i] == rxdataNumbers.products[i]);
	}

}

void test_storing_structure_60_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		unsigned int numbers[10]; 
		UInt8 magic[10];  
		UInt8 products[10]; 
	}t_numbers; 
	int i = 0; 
	gpNvm_Result result;
	UInt8 Txbuffer[60];
	t_numbers dataNumbers;
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEEDDEAD; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	for(i=0; i<9; i++)
	{
		dataNumbers.products[i] = 0x10; 
	}
	memset(Txbuffer,0xff,60);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result= gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer);
	assert(result == NVM_SUCCESS);
}

void test_data_retrieval_60_bytes()
{
	typedef struct __attribute__((packed)) numbers {
		unsigned int numbers[10]; 
		UInt8 magic[10]; 
		UInt8 products[10];  
	}t_numbers; 
	int i = 0; 
	t_numbers dataNumbers;
	t_numbers rxdataNumbers; 
	gpNvm_Result result;
	UInt8 Txbuffer[60];
	UInt8 rxDataSize = 0x0; 
	UInt8 rxBuffer[60] = {0}; 
	UInt8 dataSize = (UInt8)(sizeof(dataNumbers));
	for(i=0; i<9; i++)
	{
		dataNumbers.numbers[i] = 0xDEED5005; 
	} 
	for(i=0; i<9; i++)
	{
		dataNumbers.magic[i] = 0x99; 
	}
	for(i=0; i<9; i++)
	{
		dataNumbers.products[i] = 0xA0; 
	}
	memset(Txbuffer,0xff,50);
	memcpy(Txbuffer,&dataNumbers,sizeof(dataNumbers));
	result= gpNvm_SetAttribute(++t_attribute_id,dataSize,Txbuffer); 
	assert(result == NVM_SUCCESS);

	result = gpNvm_GetAttribute(t_attribute_id, &rxDataSize, rxBuffer); 
	assert(result == NVM_SUCCESS); 
	assert(rxDataSize == dataSize); 
	memcpy(&rxdataNumbers,rxBuffer,sizeof(rxdataNumbers)); 
	/*Perform the data sanity check */
	for(i=0; i<9; i++)
	{
		assert(dataNumbers.numbers[i] == rxdataNumbers.numbers[i]); 
		assert(dataNumbers.magic[i] == rxdataNumbers.magic[i]); 
		assert(dataNumbers.products[i] == rxdataNumbers.products[i]);
	}

}
/* 
 * This is an application driver which is going to use
 * driver APIs to store 256-Bytes of data into the 
 * NVM and try to retrieve 256-Bytes back 
 *
 * There would be some variables which would be used 
 * as a common for these example drivers 
 *
 * test_example_driver_1() would just store 256 bytes
 * into the memory 
 *
 * test_example_driver_2() would read these 256 bytes
 * from the memory and compares the data
 *
 */

UInt8 driver_address = 0x0; 

void test_example_driver_1()
{
	UInt8 Txbuffer[61]; 
	UInt8* AppMemory = NULL;
	/*keep track of index from where we have to read and verify data*/
	driver_address = t_attribute_id; 
	gpNvm_Result result; 
	int i =0;
	/*Application memory which is from Heap*/
	AppMemory = (UInt8*)calloc(256,sizeof(UInt8)); 
	assert(NULL != AppMemory);
	/*Fill the application memory with some data*/
	memset(AppMemory,0xAB,256); 
	/*Driver can only take 61 bytes at a time, which means 256 gets*/
	/* divided as 256/61 = 61*4 + 12 bytes */
	for(i=0; i<4; i++)
	{
		memcpy(Txbuffer,(AppMemory+ (61*i)),61);
		result = gpNvm_SetAttribute(++t_attribute_id,61,Txbuffer);
		assert(result == NVM_SUCCESS);
		memset(Txbuffer,0xff,61); 
	}
	/*Plan something for the last 12 bytes */
	memset(Txbuffer,0xff,61);
	memcpy(Txbuffer, (AppMemory+244), 12); 
	result = gpNvm_SetAttribute(++t_attribute_id,12,Txbuffer);
	assert(result == NVM_SUCCESS);
	free(AppMemory);
}

void test_example_driver_2()
{
	UInt8 Rxbuffer[61];
	UInt8* AppMemory = NULL; 
	UInt8 SizeReceived = 0x0;
	gpNvm_Result result;
	int i =0; 
	/* Get the Application memory from the heap*/
	AppMemory = (UInt8*)calloc(256,sizeof(UInt8));
	assert(NULL != AppMemory); 

	/*Fill the App memory with junk data so you can later*/
	/* on check if the right data is available when you*/ 
	/* read it from the driver */
	memset(AppMemory, 0xFF, 256);
	/* Start reading the data from the storage*/
	/* Since we need to read 256 Bytes of data, we have to */
	/* use the same strategy as before 61*4 + 12bytes */ 
	for(i=0;i<4; i++)
	{
		result = gpNvm_GetAttribute(++driver_address,&SizeReceived,Rxbuffer);
		assert(result == NVM_SUCCESS);
		assert(SizeReceived == 61);
		memcpy((AppMemory+ (61*i)),Rxbuffer,61);
	}
	result = gpNvm_GetAttribute(++driver_address,&SizeReceived,Rxbuffer);
	assert(result == NVM_SUCCESS);
	assert(SizeReceived == 12);
	memcpy((AppMemory+244),Rxbuffer, 12);
	for(i=0; i<255; i++)
	{
		assert(AppMemory[i] == 0xAB);
	}
	free(AppMemory);
}

/*
 * Next section contains test cases which are non-functional 
 * test cases. This means we shall check if the driver returns
 * the necessary erros as was specified
 *
 */ 
void test_incorrect_packet_size_storage()
{
	UInt8* AppMemory = NULL; 
	gpNvm_Result result; 
	AppMemory = (UInt8*)calloc(64,sizeof(UInt8));
	assert(NULL != AppMemory);
	memset(AppMemory,0xAB,64);
	result = gpNvm_SetAttribute(++t_attribute_id,64,AppMemory);
	assert(result == NVM_ERROR_PACKET_SIZE_INCORRECT);
}
void test_storing_data_with_zero_length()
{
	UInt8* AppMemory = NULL; 
	gpNvm_Result result; 
	AppMemory = (UInt8*)calloc(64,sizeof(UInt8));
	assert(NULL != AppMemory);
	memset(AppMemory,0xAB,64);
	result = gpNvm_SetAttribute(++t_attribute_id,0x0,AppMemory);
	assert(result == NVM_SUCCESS);
}
void test_storing_data_with_buffer_null()
{
	UInt8* AppMemory = NULL; 
	gpNvm_Result result; 
	result = gpNvm_SetAttribute(++t_attribute_id,60,AppMemory);
	assert(result == NVM_NULL_POINTER_SUPPLIED);
}

void test_try_overwriting_data()
{
	UInt8* AppMemory = NULL; 
	gpNvm_Result result; 
	AppMemory = (UInt8*)calloc(64,sizeof(UInt8));
	assert(NULL != AppMemory);
	memset(AppMemory,0xAB,64);
	/*Hard code the AttrId here from the previous ones*/
	result = gpNvm_SetAttribute(0x2,60,AppMemory);
	assert(result == NVM_WRITE_ERROR_PAGE_OCCUPIED);	
}

void test_retrieval_with_null_pointers()
{
	UInt8 Rxbuffer[61];
	UInt8* AppMemory = NULL; 
	UInt8 SizeReceived = 0x0;
	gpNvm_Result result;
	/* Get the Application memory from the heap*/
	AppMemory = (UInt8*)calloc(256,sizeof(UInt8));
	assert(NULL != AppMemory);
	result = gpNvm_GetAttribute(++driver_address,NULL,Rxbuffer);
	assert(result == NVM_NULL_POINTER_SUPPLIED); 
	result = gpNvm_GetAttribute(++driver_address,&SizeReceived,NULL);
	assert(result == NVM_NULL_POINTER_SUPPLIED); 
	result = gpNvm_GetAttribute(++driver_address,NULL,NULL);
	assert(result == NVM_NULL_POINTER_SUPPLIED); 
}

void test_retrieval_from_memory_which_is_unavailable()
{
	UInt8 Rxbuffer[61];
	UInt8* AppMemory = NULL; 
	UInt8 SizeReceived = 0x0;
	gpNvm_Result result;
	/* Get the Application memory from the heap*/
	AppMemory = (UInt8*)calloc(256,sizeof(UInt8));
	assert(NULL != AppMemory);
	result = gpNvm_GetAttribute(0xfe,&SizeReceived,Rxbuffer);
	assert(result == NVM_READ_NOT_POSSIBLE_EMPTY); 
}

void test_checksum_corruption()
{
	UInt8 Rxbuffer[61]; 
	UInt8 corrupt_checksum = 0xab; 
	gpNvm_Result result = NVM_SUCCESS; 
	FILE *corruption_ptr;
	UInt8 attrIdToCorrupt = 0x15; 
	int i = 0; 
	long int offset = 0x0; 
	UInt8 SizeReceived = 0x0; 

	/*do a corruption of the crc*/
	corruption_ptr = fopen("nvm.bin","r+b");
	assert(NULL != corruption_ptr); 
	for(i=0; i< attrIdToCorrupt; i++)
	{
		offset+= 0x40; 
	}
	offset+=63; /* Move the offset to location*/
	fseek(corruption_ptr,offset,SEEK_SET);
	fwrite(&corrupt_checksum,sizeof(corrupt_checksum),0x1, corruption_ptr);
	fclose(corruption_ptr);

	/*Now perform a read of data*/
	result = gpNvm_GetAttribute(attrIdToCorrupt,&SizeReceived,Rxbuffer);
	assert(result == NVM_ERROR_CHECKSUM_CORRUPT); 
}

/*In this test case we will first fix the checksum which we corrupted*/
/*in the previous test case and then we will corrupt the data */
/* to see if the checksum fails again */ 
void test_data_corruption_in_memory()
{
	UInt8 Rxbuffer[61]; 
	UInt8 correct_checksum = 0x04;
	UInt8 corrupt_data_buffer[2] = {0xBE,0xEF};  
	gpNvm_Result result = NVM_SUCCESS; 
	FILE *corruption_ptr;
	UInt8 attrIdToCorrupt = 0x15; 
	int i = 0; 
	long int offset = 0x0; 
	UInt8 SizeReceived = 0x0; 

	/*do a corruption of the crc*/
	corruption_ptr = fopen("nvm.bin","r+b");
	assert(NULL != corruption_ptr); 
	for(i=0; i< attrIdToCorrupt; i++)
	{
		offset+= 0x40; 
	}
	/*Corrupt data bytes*/
	offset+= 3; 
	fseek(corruption_ptr,offset,SEEK_SET);
	fwrite(corrupt_data_buffer,sizeof(corrupt_data_buffer),0x1,corruption_ptr);
	/*Fix the broken CRC*/
	offset+=60; 
	fseek(corruption_ptr,offset,SEEK_SET);
	fwrite(&correct_checksum,sizeof(correct_checksum),0x1, corruption_ptr);
	fclose(corruption_ptr);

	/*Now perform a read of data*/
	result = gpNvm_GetAttribute(attrIdToCorrupt,&SizeReceived,Rxbuffer);
	assert(result == NVM_ERROR_CHECKSUM_CORRUPT); 
}
